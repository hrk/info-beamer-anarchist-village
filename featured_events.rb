#!/usr/bin/env ruby

require 'json'
require 'net/http'
require 'uri'
require 'date'

featured_room = 'Anarchist Village'

# guids of featured sessions
uri = URI.parse('https://public.etherpad-mozilla.org/p/apv-talks/export/txt')
response = Net::HTTP.get_response uri
featured_guids = response.body

# guids of featured sessions
uri = URI.parse('https://public.etherpad-mozilla.org/p/apv-rooms/export/txt')
response = Net::HTTP.get_response uri
other_rooms = response.body


# use automatic dumps of all events
#uri = URI.parse('http://data.c3voc.de/32C3/everything.schedule.json')
#response = Net::HTTP.get_response uri
#raw = response.body

# cached json for development
raw = File.read('examples/everything.schedule.json')

all = JSON.parse(raw)

days=all['schedule']['conference']['days']

featured = Array.new

days.each do |day|
  rooms = day['rooms']
  rooms.each do |room,sessions|
    sessions.each do |session,hash|
      guid = session['guid']
	  if room == featured_room or featured_guids.match(guid) or other_rooms.match(room)
        # make data compatible with info-beamer-nodes/30c3-room config
        session['place'] = session['room']
        session['speakers'] = Array.new
        session['persons'].each do | person |
          session['speakers'].push person['public_name']
        end

        date = DateTime.iso8601(session['date'])
        session['unix'] = Time.at(date.to_time).to_i

        featured << '  ' + session.to_json
      end
    end
  end
end

output = featured.join ', '
puts '[ ' + output + ']'
