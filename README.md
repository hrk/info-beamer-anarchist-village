# Info-Beamer config for anarachist village

# Workflow

In order to info-beam events, we should

- Register self-organized sessions
- Each session gets a guid in the wiki (see http://data.c3voc.de/32C3/everything.schedule.json)
- guids.featured.txt holds all relevant events (talks or self-org. sessions)
  we'd like to promote
- using this file as input, `featured_events.rb` will output json of selected
  events.
- This can be used for input source for info-beamer (todo)

# Todo

- use id's instead of guids ?
- Pull favorite ids from everybodies [halfnarp](http://halfnarp.events.ccc.de) url,
  i.e. http://halfnarp.events.ccc.de/#eaa0604c4f5612dc78b40ac941dc124980cfae9b4984730a40f59849894f369f
  json is at  http://halfnarp.events.ccc.de/-/talkpreferences/public/eaa0604c4f5612dc78b40ac941dc124980cfae9b4984730a40f59849894f369f

## Get json of featured sessions

    ln -s examples/guids.featured.example.txt guids.featured.txt
    ./featured_events.rb > info-beamer-config-example/schedule.json

## Run info-beamer

    info-beamer ./info-beamer-config-example &

### fake time to preview

    ./info-beamer-config-example/service


# Links

Other ways to scrape the events from the wiki:

- https://events.ccc.de/congress/2015/wiki/Static:Crawling
- https://github.com/voc/schedule/blob/master/wiki2schedule_32C3.py
- http://data.c3voc.de/32C3/
- https://erdgeist.org/halfnarp/
- https://events.ccc.de/congress/2015/wiki/Special:Ask
